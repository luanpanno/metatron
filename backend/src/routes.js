const { Router } = require('express');

const UsuarioController = require('./controllers/UsuarioController');
const CategoriaController = require('./controllers/CategoriaController');
const ListaController = require('./controllers/ListaController');
const TarefaController = require('./controllers/TarefaController');

const routes = Router();

routes.get('/', (req, res) => {
    res.send('API para teste da Metatron!');
});

routes.get('/usuarios', UsuarioController.index);
routes.post('/usuarios', UsuarioController.store);

routes.get('/categorias', CategoriaController.index);
routes.get('/categorias/:id', CategoriaController.show);
routes.post('/categorias', CategoriaController.store);
routes.delete('/categorias', CategoriaController.destroy);

routes.get('/listas', ListaController.index);
routes.get('/listas/:id', ListaController.show);
routes.post('/listas', ListaController.store);
routes.put('/listas/:id', ListaController.update);
routes.put('/listas/concluida/:id', ListaController.updateConcluido);
routes.delete('/listas/:id', ListaController.destroy);

routes.get('/tarefas', TarefaController.index);
routes.get('/tarefas/listas/:id', TarefaController.showListas);
routes.get('/tarefas/usuarios/:id', TarefaController.showUsuarios);
routes.post('/tarefas', TarefaController.store);
routes.put('/tarefas/:id', TarefaController.update);
routes.put('/tarefas/concluida/:id', TarefaController.updateConcluido);
routes.delete('/tarefas/:id', TarefaController.destroy);
routes.delete('/tarefas/listas/:listaId', TarefaController.destroyByList);

module.exports = routes;
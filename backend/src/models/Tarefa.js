const mongoose = require('mongoose');

const TarefaSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    listaId: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    concluida: {
        type: Boolean,
        default: false
    },
    usuarioId: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Tarefa', TarefaSchema);
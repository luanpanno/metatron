const mongoose = require('mongoose');

const CategoriaSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Categoria', CategoriaSchema);
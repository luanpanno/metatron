const mongoose = require('mongoose');

const ListaSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true
    },
    categoriaId: {
        type: Number,
        default: 0
    },
    concluida: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Lista', ListaSchema);
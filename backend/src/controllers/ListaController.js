const Lista = require('../models/Lista');

module.exports = {
    async index(req, res){
        const lista = await Lista.find();

        res.json(lista);
    },

    async show(req, res){
        const { id } = req.params;

        const lista = await Lista.findOne({id});

        res.json(lista);
    },

    async store(req, res){
        const { id, nome, categoriaId, concluida } = req.body;

        const lista = new Lista({
            id,
            nome,
            categoriaId,
            concluida
        });

        try{
            const listaSalva = await lista.save();

            res.json(listaSalva);
        } catch(err){
            res.json({message: err});
        }
        
    },

    async update(req, res){
        const { id } = req.params;

        const { novoNome, novaCategoria } = req.body;

        const lista = await Lista.findOne({ id });

        const listaAtualizada = await Lista.updateOne({id}, {nome: novoNome, categoriaId: novaCategoria});

        res.json(listaAtualizada);
    },

    async updateConcluido(req, res){
        const { id } = req.params;

        const { novoEstado } = req.body;

        const lista = await Lista.findOne({ id });

        const listaAtualizada = await Lista.updateOne({id}, {concluida: novoEstado});

        res.json(listaAtualizada);
    },

    async destroy(req, res){
        const { id } = req.params;

        await Lista.deleteOne({ id });

        return res.json('deleted');
    }
}
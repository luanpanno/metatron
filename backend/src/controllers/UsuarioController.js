const Usuario = require('../models/Usuario');

module.exports = {
    async index(req, res){
        const usuarios = await Usuario.find();

        res.json(usuarios);
    },

    async store(req, res){
        const { nome, email, senha, id } = req.body;

        const usuarios = await Usuario.findOne({ email });

        const usuario = new Usuario({
            nome,
            email,
            senha,
            id
        });

        try{
            if(!usuarios){
                const usuarioSalvo = await usuario.save();
    
                res.status(201).json(usuarioSalvo);
            } else{
                res.json({message: 'Email já cadastrado.'})
            }
        } catch(err){
            res.json({message: err});
        }
        
    }
}
const Categoria = require('../models/Categoria');

module.exports = {
    async index(req, res){
        const categoria = await Categoria.find();

        res.json(categoria);
    },

    async show(req, res){
        const { id } = req.params;

        const categoria = await Categoria.findOne({id});

        res.json(categoria);
    },

    async store(req, res){
        const { nome, id } = req.body;

        const categorias = await Categoria.findOne({ nome });

        const categoria = new Categoria({
            nome,
            id
        });

        try{
            if(!categorias){
                const categoriaSalva = await categoria.save();
    
                res.status(201).json(categoriaSalva);
            }
        } catch(err){
            res.json({message: err});
        }
        
    },

    async destroy(req, res){
        // const { id } = req.params;

        await Categoria.deleteOne();

        return res.json('deleted');
    }
}
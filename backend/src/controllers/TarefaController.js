const Tarefa = require('../models/Tarefa');

module.exports = {
    async index(req, res){
        const tarefa = await Tarefa.find();

        res.json(tarefa);
    },

    async showListas(req, res){
        const { id } = req.params;

        const tarefa = await Tarefa.find({listaId: id});

        res.json(tarefa);
    },

    async showUsuarios(req, res){
        const { id } = req.params;

        const tarefa = await Tarefa.find({usuarioId: id});

        res.json(tarefa);
    },

    async store(req, res){
        const { id, listaId, nome, concluida, usuarioId } = req.body;

        const tarefa = new Tarefa({
            id, 
            listaId, 
            nome, 
            concluida, 
            usuarioId
        });

        try{
            const tarefaSalva = await tarefa.save();

            res.status(201).json(tarefaSalva);
        } catch(err){
            res.json({message: err});
        }
        
    },

    async update(req, res){
        const { id } = req.params;

        const { novoNome } = req.body;

        const tarefa = await Tarefa.findOne({ id });

        const novaTarefa = await Tarefa.updateOne({id}, {nome: novoNome});

        res.json(novaTarefa);
    },
    
    async updateConcluido(req, res){
        const { id } = req.params;

        const { novoEstado } = req.body;

        const tarefa = await Tarefa.findOne({ id });

        const novaTarefa = await Tarefa.updateOne({id}, {concluida: novoEstado});

        res.json(novaTarefa);
    },

    async destroy(req, res){
        const { id } = req.params;

        await Tarefa.deleteOne({ id });

        return res.json('deleted');
    },
    
    async destroyByList(req, res){
        const { listaId } = req.params;

        await Tarefa.deleteMany({ listaId });

        return res.json('deleted');
    }
}
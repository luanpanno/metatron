const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

const routes = require('./routes')

const PORT = 3001;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(routes);

mongoose.connect('mongodb://localhost:27017/metatron', { useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => {  console.log('MongoDB Connected…')})
    .catch(err => console.log(err));

app.listen(PORT, () => {
    console.log(`Server de pé em: http://localhost:${PORT} `);
    console.log('Para derrubar o servidor aperte CTRL + C');
});
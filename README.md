# Metatron - Prova da Equipe .NET
Feito por Luan Luiz Panno Cabral

## 1. Tecnologias Utilizadas
* Node.js e MongoDB no BackEnd
* ReactJS para o FrontEnd

## 2. Bibliotecas
### BackEnd
* **Express** - Utilizado para criação do server.
* **Body Parser** - Utilizado ara converter os dados da requisição.
* **Cors** - Utilizado para permitir o acesso à API.
* **Mongoose** - Utilizado para manipulação do MongoDB no Node facilmente.

### FrontEnd
* **Styled Components** - Utilizei o styled components pois é uma ótima maneira de estilizar e é uma ótima combinação com o React por ser CSS in JS.
* **React Router** - React Router para criar as rotas de Login, Register e da tela principal do Todo.
* **Fontawesome React** - Uma biblioteca ótima para usar ícones no React, e de fácil utilização.
* **Axios** - Para comunicação com a api.

## 3. Como foi feito
Comecei o teste pelo FrontEnd, fazendo primeiramente a tela de Login e de Registro por serem as mais simples de serem feitas e logo depois comecei a criar o Todo. Primeiro fiz a função de adicionar lista, depois os todos dentro das listas e por último a função de pesquisar

Após o FrontEnd feito, comecei a construir a API, criando o server com o Express, linkando o MongoDB, criando os models e o controllers da aplicação e finalizando com as rotas para utilização da API

Depois do BackEnd feito, precisei consumir a API no FrontEnd, então fui apenas trocando as partes onde a API se encaixava e adicionando as novas funções de Login, Registro e outras envolvendo as listas e os todos

## 4. Quais foram as dificuldades
Acredito que minha maior dificuldade foi o nervosismo, por conta de ser meu primeiro teste. Mas o desenvolvimento fluiu de uma forma natural. Ainda fazendo o teste, aprendi e reparei detalhes que farão diferença no futuro na forma em que eu desenvolvo!

## 5. Como executar
### BackEnd
Dentro da pasta Backend, instale as dependências com

    npm install

depois das dependências instaladas, digite para iniciar: 

    npm start

**OBS**: Verifique a url do mongoose conect em index.js

    mongoose.connect('mongodb://localhost:27017/metatron', (...));

### FrontEnd
Dentro da pasta Frontend, instale as dependências com

    npm install

depois das dependências instaladas, digite para iniciar:

    npm start

## 6. Aplicação
![Tela Login](https://i.imgur.com/FjJvx5c.png)

![Tela Registro](https://i.imgur.com/SHuF0rw.png)

![Tela Principal](https://i.imgur.com/ier02XM.png)

![Tela Todo](https://i.imgur.com/ROP5zwc.png)
import React, { useState } from 'react';
import styled from 'styled-components';

import { ListsProvider } from '../contexts/ListsContext';

import Header from '../components/Header';
import ListForm from '../components/ListForm';
import Lists from '../components/Lists';

function App() {
  const [search, setSearch] = useState({});

  return (
    <ListsProvider>
      <Div>
        <Header />
        <main>
          <ListForm handleSearch={setSearch} />
          <div className='listas'>
            <Lists search={search} />
          </div>
        </main>
      </Div>
    </ListsProvider>
  );
}

const Div = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100vh;

  main{
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    .listas{
      display: flex;
      flex-wrap: wrap;
    }
  }
`;

export default App;

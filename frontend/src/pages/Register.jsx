import React, { useEffect, useState } from 'react';

import api from '../services/api';

import { UserForm, UsersInput, MainButton } from '../components/Styles';

import Header from '../components/Header';

const Register = props => {
    const [user, setUser] = useState({});

    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');

    useEffect(() => {
        handleRegister(user);
    }, [user]);

    async function handleRegister(data){
        const response = await api.post('/usuarios', data);

        if(response.status === 201){
            alert('Cadastro realizado!');
        }
    }
    
    function handleInputName(e){
        setNome(e.target.value);
    }
    
    function handleInputEmail(e){
        setEmail(e.target.value);
    }
    
    function handleInputPassword(e){
        setSenha(e.target.value);
    }
    
    async function handleRegisterButton(e){
        e.preventDefault();
        const users = await api.get('/usuarios');
        setUser({nome, email, senha, id: users.data.length+1});
        setNome('');
        setEmail('');
        setSenha('');
    }

    return (
        <UserForm>
            <Header />
            <form>
                <UsersInput value={nome} onChange={handleInputName} type="text" placeholder="Nome" />
                <UsersInput value={email} onChange={handleInputEmail} type="email" placeholder="Email" />
                <UsersInput value={senha} onChange={handleInputPassword} type="password" placeholder="Senha" />
                <MainButton color='#499FF2' onClick={handleRegisterButton}>Register</MainButton>
                <p>Já possui conta? <a href="/login">Login</a></p>
            </form>
        </UserForm>
    );
}

export default Register;
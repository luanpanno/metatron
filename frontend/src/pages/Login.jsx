import React, { useState, useEffect, useContext } from 'react';

import { UserForm, UsersInput, MainButton } from '../components/Styles';

import { UserContext } from '../contexts/UserContext';

import Header from '../components/Header';
import api from '../services/api';

const Login = props => {
    const userContext = useContext(UserContext);

    const [users, setUsers] = useState([]);

    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');

    useEffect(() => {
        async function loadUsers(){
            const response = await api.get('/usuarios');

            setUsers(response.data);
        }
        loadUsers();
    }, []);

    function handleInputEmail(e){
        setEmail(e.target.value);
    }

    function handleInputSenha(e){
        setSenha(e.target.value);
    }

    function handleClick(e){
        e.preventDefault();
        checkLogin();
    }

    function checkLogin(){
        let userFiltered = users.filter(user => user.email === email && user.senha === senha);
        if(userFiltered.length > 0){
            userContext.setLoggedState(true);
            userContext.addUser(userFiltered[0]);
            return null;
        } else{
            return alert('Email ou senha inválidos');
        }
    }

    return (
        <UserForm>
            <Header />
            <form>
                <UsersInput onChange={handleInputEmail} type="email" placeholder="Email" />
                <UsersInput onChange={handleInputSenha} type="password" placeholder="Senha" />
                <MainButton color='#499FF2' onClick={handleClick}>Login</MainButton>
                <p>Não tem conta? <a href="/register">Registrar</a></p>
            </form>
        </UserForm>
    );
}

export default Login;
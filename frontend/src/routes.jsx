import React, { useContext } from 'react';
import { Route, BrowserRouter, Redirect, Switch } from 'react-router-dom';

import { UserContext, UserProvider } from './contexts/UserContext';

import TodoApp from './pages/TodoApp';
import Login from './pages/Login';
import Register from './pages/Register';

const TodoRoute = ({ component: Component, ...rest}) => {
    const userLogged = useContext(UserContext);

    return (
        <Route 
            {...rest}
            render={props => (
                userLogged.logged 
                ? <Component {...props} />
                : <Redirect to={{pathname: '/login', state: { from: props.location }}} />
            )}
        />
    );
}

const Logged = ({ component: Component, ...rest}) => {
    const userLogged = useContext(UserContext);

    return (
        <Route 
            {...rest}
            render={props => (
                userLogged.logged 
                    ? <Redirect to={{pathname: '/', state: { from: props.location }}} />
                    : <Component {...props} />
            )}
        />
    );
}

const Routes = () => {
    return (
        <UserProvider>
            <BrowserRouter>
                <Switch>
                    <TodoRoute path="/" exact={true} component={TodoApp} />
                    <Logged path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                </Switch>
            </BrowserRouter>
        </UserProvider>
    );
}

export default Routes;
import React, { useState, createContext, useEffect } from 'react';
import api from '../services/api';

export const ListsContext = createContext();

export const ListsProvider = props => {
    const [lists, setLists] = useState([]);
    const [user, setUser] = useState({});

    const [listasApi, setListasApi] = useState([]);

    useEffect(() => {
        loadListas();
    }, [lists]);


    async function loadListas(){
        const response = await api.get('/listas');

        setListasApi(response.data);
    }

    function addUser(user){
        setUser(user);
    }

    function addList(list){
        if(Object.keys(list).length === 0 && list.constructor === Object) return null;
        const newList = {
            id: list.id,
            nome: list.nome,
            categoriaId: list.categoriaId,
            concluida: false,
        }
        setLists([...lists, newList]);
      }
    
      function deleteList(list){
        lists.splice(lists.indexOf(list), 1);
        setLists([...lists]);
    }

    return (
        <ListsContext.Provider value={{lists, listasApi, addList, deleteList, user, addUser}}>
            {props.children}
        </ListsContext.Provider>
    );
}

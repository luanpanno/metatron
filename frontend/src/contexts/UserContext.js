import React, { useState, createContext } from 'react';

export const UserContext = createContext();

export const UserProvider = props => {
    const [user, setUser] = useState({});
    const [logged, setLogged] = useState(false);

    function addUser(data){
        setUser(data);
    }

    function setLoggedState(state){
        setLogged(state);
    }

    return (
        <UserContext.Provider value={{ user, addUser, logged, setLoggedState }}>
            {props.children}
        </UserContext.Provider>
    );
}
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faTrash, faPencilAlt, faCheck } from '@fortawesome/free-solid-svg-icons';

import api from '../services/api';

import { MainButton } from './Styles'

import Todos from './Todos';
import EditPopup from './EditPopup';

const Card = props => {
    const [openTodo, setOpenTodo] = useState(false);
    const [openEdit, setOpenEdit ] = useState(false);

    const [done, setDone] = useState(props.list.concluida);
    const [categoria, setCategoria] = useState('');

    useEffect(() => {
        pegarCategoria(props.list.categoriaId);
    }, []);

    function handleDelete(){
        alert('Deletado!');
        deleteListApi(props.list.id);
        deleteTarefasApi(props.list.id);
        deleteList(props.list);
    }

    function deleteList(list){
        props.listas.splice(props.listas.indexOf(list), 1);
        props.setListas([...props.listas]);
    }

    async function deleteListApi(id){
        return await api.delete(`/listas/${id}`);
    }

    async function deleteTarefasApi(listId){
        return await api.delete(`/tarefas/listas/${listId}`);
    }

    function deleteList(list){
        props.listas.splice(props.listas.indexOf(list), 1);
        props.setListas([...props.listas]);
    }

    function handleDone(){
        atualizarConcluida(props.list.id, true);
        setDone(true);
    }

    async function atualizarConcluida(id, novoEstado){
        return await api.put(`/listas/concluida/${id}`, {novoEstado});
    }

    async function pegarCategoria(data){
        const response = await api.get(`/categorias/${data}`);

        if(response.data !== null) setCategoria(response.data.nome);
    }

    return (
        <>
            <Div>
                {done
                    ? <p>Concluído <FontAwesomeIcon icon={faCheck} /> </p>
                    : null
                }
                <h1>
                    {props.list.nome}
                </h1>
                {(props.list.categoriaId && categoria !== '')
                    ? <p className="tag">{categoria}</p>
                    : null
                }
                <div>
                    <MainButton title="Marcar como concluído" color='#aaa' onClick={handleDone} >
                        <FontAwesomeIcon icon={faCheck} />
                    </MainButton>
                    <MainButton title="Editar lista" color='#1fbf00' onClick={setOpenEdit}>
                        <FontAwesomeIcon icon={faPencilAlt} />
                    </MainButton>
                    <MainButton title="Excluir lista" color='#f00' onClick={handleDelete}>
                        <FontAwesomeIcon icon={faTrash} />
                    </MainButton>
                    <MainButton title="Ver lista" color='#499FF2' onClick={setOpenTodo}>
                        <FontAwesomeIcon icon={faEye} />
                    </MainButton>
                </div>
            </Div>
            {openTodo ? <Todos handleOpenTodo={setOpenTodo} list={props.list} /> : null}
            {openEdit ? <EditPopup handleOpenEdit={setOpenEdit} listas={props.listas} list={props.list} categoria={setCategoria} /> : null}
        </>
    );
}

const Div = styled.div`
    background-color: #fafafa;
    box-shadow: 0px 1px 10px 1px #ccc;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    border-radius: 10px;
    padding: 15px;
    margin: 20px;
    max-width: 450px;

    p{
        color: #499FF2;
        margin: 5px 0;
    }

    h1{
        text-transform: uppercase;
        font-size: 1.4rem;
        font-weight: 500;
    }

    .tag{
        color: #ffd800;
        border: 1px solid #ffd800;
        padding: 5px;
        margin: 5px;
        background-color: #fff;
    }

    input{
        margin: 10px 0;
    }
`;

export default Card;
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import api from '../services/api';

import { Popup, CloseButton } from './Styles';

import TodoForm from './TodoForm';
import TodoList from './TodoList';

const Todo = props => {
    const [tarefas, setTarefas] = useState({});

    useEffect(() => {
        pegarTarefas(props.list.id);
    });

    async function pegarTarefas(id){
        const response = await api.get(`/tarefas/listas/${id}`);

        setTarefas(response.data);
    }

    return (
        <Popup>
            <App>
                <TodoForm list={props.list} />
                {
                    (tarefas.length) 
                        ? <TodoList todos={tarefas} />
                        : null
                }
                <CloseButton onClick={() => props.handleOpenTodo(false) }>
                    <FontAwesomeIcon icon={faTimes} />
                </CloseButton>
            </App>
        </Popup>
    );
}

const App = styled.div`
    background-color: #fff;
    box-shadow: 0px 1px 10px 5px #ccc;
    padding: 20px;
    border-radius: 5px;
    position: relative;
`;

export default Todo;
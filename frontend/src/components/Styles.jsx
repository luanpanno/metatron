import styled from 'styled-components';

export const MainButton = styled.button`
    background-color: ${props => props.color};
    border: none;
    border-radius: 5px;
    padding: 5px 10px;
    color: white;
    font-size: 1rem;
    cursor: pointer;
    transition: all 200ms;
    margin: 5px;
`;

export const CloseButton = styled.button`
    position: absolute;
    top: -10px;
    right: -10px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 1.2rem;
    width: 25px;
    height: 25px;
    border: 2px solid #6e0000;
    border-radius: 100%;
    color: white;
    background-color: #ff5353;
    cursor: pointer;
`;

export const Popup = styled.div`
    width: 100vw;
    height: 100vh;
    background-color: rgba(255, 255, 255, .8);
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

export const Window = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 10px;
    border: 1px solid #eee;
    background-color: #fff;
    width: 350px;

    ul{
        display: flex;
        flex-wrap: wrap;

        li{
            color: #ffd800;
            border: 1px solid #ffd800;
            padding: 5px;
            margin: 5px;
            background-color: #fff;
        }
    }

    input{
        font-size: 1.2rem;
        width: 100%;
        margin: 5px 0;
    }
`;

export const UsersInput = styled.input`
    margin: 5px 0;
    padding: 10px;
    font-size: 1.2rem;
    width: 300px;
`;

export const UserForm = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100vw;
    height: 100vh;

    form{
        border: 1px solid #ccc;
        border-radius: 5px;
        display: flex;
        flex-direction: column;
        align-content: center;
        justify-content: space-between;
        padding: 25px;
        text-align: center;
        color: #333;

        p{
            margin-top: 15px;
        }
    }

    a{
        text-decoration: none;
        color: #397FBF;

        &:hover{
            text-decoration: underline;
        }   
    }
`;
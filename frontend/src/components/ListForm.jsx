import React, { useState, useContext } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPlus } from '@fortawesome/free-solid-svg-icons';

import { ListsContext } from '../contexts/ListsContext';

import AddPopup from './AddPopup';

const TodoForm = props => {
    const listsContext = useContext(ListsContext);

    const [query, setQuery] = useState('');
    const [state, setState] = useState(true);

    const [openAdd, setOpenAdd] = useState(false);
    
    function handleInputChange(e){
        setQuery(e.target.value);
    }
    
    function handleSearch(e){
        e.preventDefault();
        setState(true);
        props.handleSearch({query, state});
    }
    
    function handleAdd(e){
        e.preventDefault();
        setOpenAdd(true);
    }

    return (
        <>
            <Form>
                <input onChange={handleInputChange} value={query} placeholder="Adicionar ou pesquisar" type="text" />
                <button onClick={handleSearch}><FontAwesomeIcon icon={faSearch} /></button>
                <button onClick={handleAdd}><FontAwesomeIcon icon={faPlus} /></button>
            </Form>
            { openAdd ? <AddPopup query={query} addList={listsContext.addList} handleAddClick={setOpenAdd} /> : null}
        </>
    );
}

const Form = styled.form`
    border: 1px solid #ddd;
    padding: 5px 10px;
    display: flex;
    position: relative;
    width: 450px;

    input{
        width: 100%;
        font-size: 1.4rem;
        border: none;
    }

    button{
        border: none;
        background-color: transparent;
        font-size: 1.2rem;
        margin: 0 5px;
        cursor: pointer;
    }
`;

export default TodoForm;
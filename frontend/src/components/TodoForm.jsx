import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

import api from '../services/api';

import { UserContext } from '../contexts/UserContext';

const TodoForm = props => {
    const userContext = useContext(UserContext);
    
    const [todo, setTodo] = useState({});
    const [task, setTask] = useState('');

    const [tarefa, setTarefa] = useState({});

    useEffect(() => {
        addTarefa(tarefa);
    }, [tarefa]);

    async function addTarefa(data){
        return await api.post('/tarefas', data);
    }
    
    function handleTaskInputChange(e){
        setTask(e.target.value);
    }
    
    async function handleSubmit(e){
        e.preventDefault();
        setTodo({task, done: false});

        const tarefas = await api.get('/tarefas');
        
        setTarefa({id: tarefas.data.length+1, listaId: props.list.id, nome: task, concluida: todo.done, usuarioId: userContext.user.id});
    }

    return(
        <Form onSubmit={handleSubmit}>
            <input 
                type="text" 
                name="task"
                value={task}
                placeholder="Insira a tarefa"
                onChange={handleTaskInputChange} />
            <button><FontAwesomeIcon icon={faPlus} /></button>
        </Form>
    );
}

const Form = styled.form`
    border: 1px solid #ddd;
    padding: 5px 10px;
    display: flex;
    position: relative;
    width: 350px;

    input{
        width: 100%;
        font-size: 1.2rem;
        border: none;
    }

    button{
        background-color: #1fbf00;
        border: none;
        color: white;
        height: 100%;
        width: 35px;
        font-size: 1.2rem;
        cursor: pointer;
        position: absolute;
        right: 0;
        top: 0;
    }
`;

export default TodoForm;
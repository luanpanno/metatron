import React, { useContext } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

import { UserContext } from '../contexts/UserContext';

import { MainButton } from './Styles';

const Header = props => {
    const userContext = useContext(UserContext);

    function handleLeaveButton(){
        userContext.addUser({});
        userContext.setLoggedState(false);
    }

    return (
        <MainHeader>
            <p className='title'>Todo List</p>
            { userContext.logged 
                ? <div>
                    <p>Seja bem-vindo, <span>{userContext.user.nome}</span>!</p>
                    <MainButton onClick={handleLeaveButton} color="red"><FontAwesomeIcon icon={faSignOutAlt} /></MainButton></div>
                : null
            }
        </MainHeader>
    );
}

const MainHeader = styled.header`
    height: 8vh;
    padding: 10px;
    width: 100vw;
    display: flex;
    align-items: center;
    justify-content: space-around;
    background-color: #555;
    position: fixed;
    top: 0;
    color: white;

    .title{
        font-size: 1.4rem;
        border: 1px solid white;
        padding: 10px;
    }

    div{
        display: flex;
        align-items: center;
        justify-content: center;

        p{
            margin-right: 10px;
            
            span{
                color: #499FF2;
            }
        }

    }
`;

export default Header;
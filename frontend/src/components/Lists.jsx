import React, { useContext, useState, useEffect } from 'react';
import styled from 'styled-components';

import api from '../services/api';

import { UserContext } from '../contexts/UserContext';
import { ListsContext } from '../contexts/ListsContext';

import ListCard from './ListCard';

const TodoForm = props => {
    const userContext = useContext(UserContext);
    const listsContext = useContext(ListsContext);

    const [tarefasUserApi, setTarefasUserApi] = useState();
    const [listasId, setListasId] = useState([]);

    const [lista, setLista] = useState({});
    const [listas, setListas] = useState([]);
    const [categorias, setCategorias] = useState();

    const [hasTask, setHasTask] = useState(false);

    // Para pegar as listas eu filtro na api as tarefas do usuário logado, das tarefas do usuario eu pego os ids das listas das tarefas e depois busco na api as listas com o id pego nesse processo. Caso o usuario logado não tenha nenhuma tarefa, hasTask será falso e irá renderizar a lista do lado cliente, se o usuario logado tiver alguma tarefa na api irá renderizar as listas dele salva na api.

    useEffect(() => {
        loadTarefasUserApi(userContext.user.id);
        loadCategorias();
    }, []);

    async function loadCategorias(){
        const response = await api.get('/categorias');
        
        setCategorias(response.data);
    }
    
    useEffect(() => {
        if(tarefasUserApi !== undefined){
            if(tarefasUserApi.length > 0){
                setListasId(tarefasUserApi.map(tarefa => tarefa.listaId));
                setHasTask(true);
            } else{
                setHasTask(false);
            }
        }
    }, [tarefasUserApi]);
    
    useEffect(() => {
        if(listasId.length > 0){
            listasId.map(lista => loadLista(lista));
        }
    }, [listasId]);
    
    useEffect(() => {
        setListas([...listas, lista]);
        if(listsContext.lists.length >= 0){
            listsContext.lists.map(list => setListas([...listas, list]));
        }
    }, [lista, listsContext.lists]);
    
    async function loadTarefasUserApi(id){
        const response = await api.get(`/tarefas/usuarios/${id}`);
        
        setTarefasUserApi(response.data);
    }

    async function loadLista(id){
        const response = await api.get(`/listas/${id}`);

        setLista(response.data);
    }

    function searchCategoria(query, lista){
        let categoriaId = lista.categoriaId;
        let filtered = '';
    
        categorias.map(categoria => {
            if(categoria.id === categoriaId && categoria.nome === query){
                filtered = categoria.nome;
            }
        });
        
        return filtered;
    }

    function List(list){
        return (
            <ListCard 
                handleOpenTodo={props.handleOpenTodo} 
                key={list.id} 
                list={list}
                listas={listas}
                deleteList={listsContext.deleteList}
                tarefas={tarefasUserApi}
                setListas={setListas}
            />
        );
    }

    function listMap(lists){
        return lists.map(list => {
            if(Object.keys(list).length === 0 && list.constructor === Object) return null;
            if(props.search.state){
                if(props.search.query === list.nome || props.search.query === searchCategoria(props.search.query, list)){
                    return List(list);
                } else if(props.search.query === ''){
                    return List(list);
                }
            } else{
                return List(list);
            }
            return null;
        });
    }

    return (
        <Div>
            {hasTask
                ? listMap(listas)
                : listMap(listsContext.lists)
            }
        </Div>
    );
}

const Div = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
`;

export default TodoForm;
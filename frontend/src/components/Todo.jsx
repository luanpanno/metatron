import React, { useState } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrash, faCheck } from '@fortawesome/free-solid-svg-icons';

import api from '../services/api';

import { MainButton } from './Styles'

const Todo = ({ todo }) => {
    const [marked, setMarked] = useState(todo.concluida);

    const [edit, setEdit] = useState(false);
    const [newTask, setNewTask] = useState('');

    function handleDeleteTodo(){
        deletarTodoApi(todo.id);
    }

    async function deletarTodoApi(id){
        return await api.delete(`/tarefas/${id}`);
    }

    function handleCheckbox(e){
        setMarked(e.target.checked);
        atualizarConcluida(todo.id, e.target.checked);
    }

    async function atualizarConcluida(id, novoEstado){
        return await api.put(`/tarefas/concluida/${id}`, {novoEstado});
    }

    function handleEdit(e){
        e.preventDefault();
        setEdit(true);
    }

    function handleInputEdit(e){
        setNewTask(e.target.value);
    }

    function handleEditEnter(e){
        if(e.key === 'Enter'){
            if(newTask === '' || !newTask.trim()) return null;
            todo.task = newTask;
            setEdit(false);
            atualizarTodoApi(todo.id, newTask);
        }
    }

    async function atualizarTodoApi(id, novoNome){
        const response = await api.put(`/tarefas/${id}`, {novoNome});

        console.log(response);
    }

    return(
        <Li done={marked}>
            <div>
                <input onChange={handleCheckbox} type="checkbox" checked={marked} />
                {edit
                    ? <input autoFocus onChange={handleInputEdit} onKeyUp={handleEditEnter} type="text"/>
                    : <span>{todo.nome}</span>
                }
                {marked 
                    ? <span><FontAwesomeIcon icon={faCheck} /></span>
                    : null
                }
            </div>
            <div>
                <MainButton color='#1fbf00' onClick={handleEdit} ><FontAwesomeIcon icon={faPencilAlt} /></MainButton>
                <MainButton color='#f00' onClick={handleDeleteTodo} ><FontAwesomeIcon icon={faTrash} /></MainButton>
            </div>
        </Li>
    );
}

const Li = styled.li`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    border-bottom: 1px solid #eee;
    padding: 15px 0;

    span{
        font-size: 1.2rem;
        margin: 0 10px;
        text-decoration: ${props => props.done ? 'line-through' : 'none'};
        color: ${props => props.done ? '#499FF2' : 'black'};
    }
`;

export default Todo;
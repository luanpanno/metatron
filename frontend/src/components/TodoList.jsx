import React from 'react';
import styled from 'styled-components';

import Todo from './Todo';

const TodoList = props => {
    return(
        <Ul>
            {props.todos.map(todo => {
                if(todo === '' || todo.length === 0){
                    return null;
                } else{
                    return (<Todo key={todo.id} todo={todo} />);
                }
            })}
        </Ul>
    );
}

const Ul = styled.ul`
    width: 100%;
    border-radius: 5px;
`;

export default TodoList;
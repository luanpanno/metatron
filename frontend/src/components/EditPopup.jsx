import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import api from '../services/api';

import { Popup, Window, MainButton, CloseButton } from './Styles';

const EditPopup = props => {
    const [categorias, setCategorias] = useState([]);
    
    const [novoNome, setNovoNome] = useState(props.list.nome);
    const [novaCategoria, setNovaCategoria] = useState('');


    useEffect(() => {
        loadCategorias();
    }, []);
    
    function handleInputName(e){
        setNovoNome(e.target.value);
    }

    function handleInputTags(e){
        setNovaCategoria(e.target.value);
    }

    function handleSubmit(e){
        e.preventDefault();

        if(novoNome === '') return null;
        
        // Pesquisa se o campo de categoria está vazio, se não estiver ele vai verificar se já existe a categoria
        if(novaCategoria){
            let categoriaFilter = categorias.filter(categoria => categoria.nome === novaCategoria);
            if(categoriaFilter.length > 0){
                editarLista(props.list.id, novoNome, categoriaFilter[0].id);
                props.list.categoriaId = categoriaFilter[0].id
            } else{
                props.list.categoriaId = categorias.length+1;
                editarLista(props.list.id, novoNome, categorias.length+1);
                addCategoria({id: categorias.length+1, nome: novaCategoria});
            }
        } else{
            editarLista(props.list.id, novoNome, 0);
        }
        
        props.list.nome = novoNome;
        props.categoria(novaCategoria);
        props.handleOpenEdit(false);
    }
    
    async function editarLista(id, novoNome, novaCategoria){
        return await api.put(`/listas/${id}`, {novoNome, novaCategoria});
    }

    async function loadCategorias(){
        const response = await api.get(`/categorias/`);

        setCategorias(response.data);
    }

    async function addCategoria(data){
        return await api.post('/categorias', data);
    }

    return (
        <Popup>
            <Window>
                <input value={novoNome} type="text" placeholder="Editar nome" onChange={handleInputName} />
                <input type="text" placeholder="Editar categorias" onChange={handleInputTags} />
                <MainButton color='#499FF2' onClick={handleSubmit}>Editar</MainButton>
                <CloseButton onClick={() => props.handleOpenEdit(false)}>
                    <FontAwesomeIcon icon={faTimes} />
                </CloseButton>
            </Window>
        </Popup>
    );
}

export default EditPopup;
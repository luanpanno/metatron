import React, { useState, useEffect, useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import api from '../services/api';

import { ListsContext } from '../contexts/ListsContext';

import { Popup, Window, MainButton, CloseButton } from './Styles';

const Todo = (props) => {
    const listsContext = useContext(ListsContext);

    const [lista, setLista] = useState({});
    const [categoria, setCategoria] = useState({});

    const [name, setName] = useState(props.query);
    const [tag, setTag] = useState('');

    const [listas, setListas] = useState([]);
    const [categorias, setCategorias] = useState([]);

    useEffect(() => {
        loadListas();
        loadCategorias();
    }, []);

    async function loadCategorias(){
        const response = await api.get('/listas');

        setCategorias(response.data);
    }

    async function loadListas(){
        const response = await api.get('/listas');

        setListas(response.data);
    }
    
    useEffect(() => {
        setListas([...listas, lista]);
        addListaApi(lista);
        listsContext.addList(lista);
    }, [lista]);


    useEffect(() => {
        addCategoria(categoria);
    }, [categoria]);

    async function addListaApi(data){
        await api.post('/listas', data);
    }

    async function addCategoria(data){
        await api.post('/categorias', data);
    }
    
    function handleSubmit(e){
        e.preventDefault();
        
        if(name === '' || !name.trim()) return null;

        let listaId = 0;
        if(listas.length > 0){
            listaId = listas[listas.length - 1].id + 1;
        } else{
            listaId = listas.length+1;
        }

        setCategoria({id: categorias.length+1, nome: tag});
        if(tag){
            let categoriaFilter = categorias.filter(categoria => categoria.nome === tag);
            if(categoriaFilter.length > 0){
                setLista({id: listaId, nome: name, categoriaId: categoriaFilter[0].id});
            } else{
                setLista({id: listaId, nome: name, categoriaId: categorias.length+1});
            }
        } else{
            setLista({id: listaId, nome: name, categoriaId: 0});
        }
        
        setTag('');
        setName('');
    }
    
    function handleNameChange(e){
        setName(e.target.value);
    }
    
    function handleTagChange(e){
        setTag(e.target.value);
    }
    
    return (
        <Popup>
            <Window>
                <input value={name} onChange={handleNameChange} type="text" placeholder="Insira o nome" />
                <input value={tag} onChange={handleTagChange} type="text" placeholder="Insira a categoria" />
                <MainButton color='#499FF2' onClick={handleSubmit}>Adicionar</MainButton>
                <CloseButton onClick={() => props.handleAddClick(false)}><FontAwesomeIcon icon={faTimes} /></CloseButton>
            </Window>
        </Popup>
    );
}

export default Todo;
